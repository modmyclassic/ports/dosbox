#!/bin/sh
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
chmod +x "dosbox"
cp -f "default.cfg" "/tmp"
echo "launch_StockUI" > "/tmp/launchfilecommand"

echo -n 2 > "/data/power/disable"
# XOTSLR01 < Possible combo
${PROJECT_ERIS_PATH}/bin/sdl_input_text_display " " 0 0 12 "/usr/share/fonts/ttf/LiberationMono-Regular.ttf" 0 0 0 "$(pwd)/win311_select.png" XOST

SELECTEDOPTION=$?

echo ${SELECTEDOPTION}
if [ "${SELECTEDOPTION}" = "100" ]; then # CROSS
  echo "Selected RUN 3.11" > "${RUNTIME_LOG_PATH}/win311.log"
  if [ ! -f "/var/volatile/launchtmp/application/WINDOWS/WIN.COM" ]; then
    ${PROJECT_ERIS_PATH}/bin/sdl_text_display "No WIN 3.11 install found! Exiting in 5 seconds..." 640 70 16 "/usr/share/fonts/ttf/LiberationMono-Regular.ttf" 0 0 0 "$(pwd)/win311splash.png"
    sleep 5
    rm -f "/tmp/default.cfg"
    exit 1
  else
    cat "application/dosbox_autoexec_win_run" >> "/tmp/default.cfg"
  fi
elif [ "${SELECTEDOPTION}" = "101" ]; then # CIRCLE
  echo "Selected RUN INSTALL" > "${RUNTIME_LOG_PATH}/win311.log"
  if [ ! -f "/var/volatile/launchtmp/application/INSTALL/SETUP.EXE" ]; then
    ${PROJECT_ERIS_PATH}/bin/sdl_text_display "No WIN 3.11 install files found! Exiting in 5 seconds..." 640 70 16 "/usr/share/fonts/ttf/LiberationMono-Regular.ttf" 0 0 0 "$(pwd)/win311splash.png"
    sleep 5
    rm -f "/tmp/default.cfg"
    exit 1
  elif [ -f "/var/volatile/launchtmp/application/WINDOWS/WIN.COM" ]; then
    ${PROJECT_ERIS_PATH}/bin/sdl_text_display "Windows 3.11 already installed, running WIN3.11 instead..." 640 70 16 "/usr/share/fonts/ttf/LiberationMono-Regular.ttf" 0 0 0 "$(pwd)/win311splash.png"
    sleep 5
    cat "application/dosbox_autoexec_win_run" >> "/tmp/default.cfg"
  else
    cat "application/dosbox_autoexec_win_install" >> "/tmp/default.cfg"
  fi
elif [ "${SELECTEDOPTION}" = "102" ]; then # SQUARE
  echo "Selected RUN DOS" > "${RUNTIME_LOG_PATH}/win311.log"
  cat "application/dosbox_autoexec" >> "/tmp/default.cfg"
elif [ "${SELECTEDOPTION}" = "103" ]; then # TRIANGLE
  echo "Selected RUN UNINSTALL" > "${RUNTIME_LOG_PATH}/win311.log"
  if [ ! -f "/var/volatile/launchtmp/application/WINDOWS/WIN.COM" ]; then
    ${PROJECT_ERIS_PATH}/bin/sdl_text_display "No WIN 3.11 install found! Exiting in 5 seconds..." 640 70 16 "/usr/share/fonts/ttf/LiberationMono-Regular.ttf" 0 0 0 "$(pwd)/win311splash.png"
    sleep 5
    rm -f "/tmp/default.cfg"
    exit 1
  else  
    cat "application/dosbox_autoexec_win_uninstall" >> "/tmp/default.cfg"
  fi
else
  echo "[ERROR] Couldn't detect controller selection result. RESULT CODE: ${SELECTEDOPTION}" > "${RUNTIME_LOG_PATH}/win311.log"
  exit 1
fi

HOME="/var/volatile/launchtmp" ./dosbox -conf "/tmp/default.cfg" -fullscreen >> "${RUNTIME_LOG_PATH}/win311.log" 2>&1
echo -n 1 > "/data/power/disable"
rm -f "/tmp/default.cfg"
